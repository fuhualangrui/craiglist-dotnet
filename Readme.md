# Minicraiglist

A few things that call for attention. Please read before you use the website.

## User Account

The website has already seeded a lot of data, including 5,000 posts, 10
categories each with 20 subcategories, 10 areas each with 5 locales and most
importantly, 20 users.

These 20 users listed below, the password is __Test123456*__. You can also register
as an ordinary user.

| Administrator | Email                        |
|---------------|------------------------------|
| True          | Jaycee_Tillman@gmail.com     |
| True          | Courtney_Friesen@hotmail.com |
| False         | Terrance_DuBuque@gmail.com   |
| True          | Myles_Carroll92@gmail.com    |
| False         | Marlene.Tromp@yahoo.com      |
| True          | Kaitlyn_Lubowitz@hotmail.com |
| False         | Ian.Crona6@gmail.com         |
| True          | Renee.Bosco@gmail.com        |
| False         | Agustina63@gmail.com         |
| False         | Christina47@gmail.com        |
| False         | Lessie52@yahoo.com           |
| False         | Cleora42@hotmail.com         |
| True          | Brian_Batz20@gmail.com       |
| True          | Branson10@hotmail.com        |
| True          | Kennith94@hotmail.com        |
| True          | Norval_Balistreri@gmail.com  |
| True          | Kyler.Frami@yahoo.com        |
| True          | Gracie_Boyer@yahoo.com       |
| True          | Johann_Jacobs36@gmail.com    |
| False         | Margret.Senger@yahoo.com     |

## Pagination

Even with 5,000 posts, in a specific subcategory or location, there are not
enough posts for pagination since there are 10,000 combinations of locations and
subcategory.

But there are enough posts for pagination in a specific *area* and *category*.
Just click the category in the homepage, posts from all the subcategories under that
cateogory would be shown. On average, with 5,000 posts and 100 combinations of *area* and *category*, there would be 50 posts.
