﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;

namespace UI.Controllers
{
    /* ------------------------------
     * Locale Related Actions:
     * - Add a locale
     * - Modify a locale
     * - Hide a locale
     * ------------------------------
     */
    [RoutePrefix("Area")]
    [UserAuthorization("admin")]
    public class LocaleController : Controller
    {
        // GET: Area/1/Locale/Add
        [Route("{areaId:int}/Locale/Add")]
        public ActionResult Add(int areaId)
        {
            return View();
        }

        // POST: Area/1/Locale/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{areaId:int}/Locale/Add")]
        public ActionResult Add(int areaId, [Bind(Include = "Name")] Location locale)
        {
            return View(locale);
        }

        // GET: Area/1/Locale/Modify/5
        [Route("{areaId:int}/Locale/Modify/{id:int}")]
        public ActionResult Modify(int areaId, int id)
        {
            return View();
        }

        //POST: Area/1/Locale/Modify/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{areaId:int}/locale/Modify/{id:int}")]
        public ActionResult Modify([Bind(Include = "Id,Name")] Location locale)
        {
            return View(locale);
        }

        // GET: Area/1/Locale/Hide/5
        [Route("{areaId:int}/Locale/Hide/{id:int}")]
        public ActionResult Hide(int areaId, int id)
        {
            return View();
        }

        // POST: Area/1/Locale/Hide/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{areaId:int}/Locale/Hide/{id:int}")]
        public ActionResult HideConfirmed(int areaId, int id)
        {
            return RedirectToAction("List", "Area");
        }
    }
}