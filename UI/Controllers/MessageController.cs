﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;
using PagedList;
using Microsoft.AspNet.Identity;

namespace UI.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private MessageDbService _service = new MessageDbService();
        private UserDbService _userService = new UserDbService();
        private PostDbService _postService = new PostDbService();

        [Authorize]
        [Route("Message/ListReceived/{user_id}/{sort_order?}")]
        public ActionResult ListReceived(string user_id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var messages = _service.GetReceivedMessageByUser(user_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.Body.Contains(searchString)
                                       || s.Title.Contains(searchString)
                                       || s.Post.Title.Contains(searchString)
                                       || s.Post.Body.Contains(searchString)
                                       || s.Receiver.Email.Contains(searchString)
                                       || s.Author.Email.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    messages = messages.OrderByDescending(s => s.PostId);
                    break;
                case "Date":
                    messages = messages.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    messages = messages.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    //messages = messages.OrderBy(s => s.PostId);
                    break;
            }
            //var list = messages.ToList();
            //int pageSize = (int)Math.Ceiling(messages.Count() / 20.0);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(messages.ToPagedList(pageNumber, pageSize));
            //return View(list);
        }



        [Authorize]
        [Route("Message/ListSent/{user_id}/{sort_order?}")]
        public ActionResult ListSent(string user_id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var messages = _service.GetSentMessageByUser(user_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.Body.Contains(searchString)
                || s.Title.Contains(searchString)
                || (s.Post != null && s.Post.Title.Contains(searchString))
                || s.Receiver.Email.Contains(searchString)
                || s.Author.Email.Contains(searchString)
                );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    messages = messages.OrderByDescending(s => s.PostId);
                    break;
                case "Date":
                    messages = messages.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    messages = messages.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    //messages = messages.OrderBy(s => s.PostId);
                    break;
            }
            //var list = messages.ToList();
            //int pageSize = (int)Math.Ceiling(messages.Count() / 20.0);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(messages.ToPagedList(pageNumber, pageSize));
            //return View(list);
        }




        [Authorize]
        [HttpGet]
        [Route("Message/MarkAllRead/{user_id}")]
        public ActionResult MarkAllRead(string user_id, bool is_all = false)
        {
            _service.MarkAllRead(user_id);
            if (is_all)
            {
                return RedirectToAction("List", new { user_id = user_id });
            }
            else
            {

                return RedirectToAction("ListUnread", new { user_id = user_id });
            }
        }




        [Authorize]
        [Route("Message/CreateManyMessages/{user_id}/{message_id}/{sort_order?}")]
        public ActionResult CreateManyMessages(int message_id, string user_id)
        {
            FakeModelGenerator.GenerateMessageForDialog(message_id);
            return RedirectToAction("Expand", new { user_id = user_id, message_id = message_id });
        }

        [Authorize]
        [Route("Message/Expand/{user_id}/{message_id}/{sort_order?}")]
        public ActionResult Expand(string user_id, int message_id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            // expand would mark as read
            var m = _service.GetMessage(message_id);
            ViewBag.Post = m.Post;
            ViewBag.PostId = m.PostId;
            if (m.AuthorId == user_id)
            {
                ViewBag.ReceiverId = m.ReceiverId;
            } else
            {
                ViewBag.ReceiverId = m.AuthorId;
            }
            if (m.ReceiverId == User.Identity.GetUserId())
            {
                m.IsRead = true;
                _service.UpdateMessage(m);
            }
            var messages = _service.GetMessageContext(message_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.Body.Contains(searchString)
                                       || s.Title.Contains(searchString)
                || (s.Post != null && s.Post.Title.Contains(searchString))
                                       || s.Receiver.Email.Contains(searchString)
                                       || s.Author.Email.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    messages = messages.OrderByDescending(s => s.PostId);
                    break;
                case "Date":
                    messages = messages.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    messages = messages.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    //messages = messages.OrderBy(s => s.PostId);
                    break;
            }

            //int pageSize = (int)Math.Ceiling(messages.Count() / 20.0);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(messages.ToPagedList(pageNumber, pageSize));
        }


        [Authorize]
        [Route("Message/MarkPageReadInUnread/{user_id}/{sort_order?}")]
        public ActionResult MarkPageReadInUnread(string user_id, string sortOrder, string currentFilter, int page)
        {
            if (page == 0)
            {
                return RedirectToAction("ListUnread");
            }
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            var searchString = currentFilter;
            ViewBag.CurrentFilter = searchString;
            var messages = _service.GetUnreadMessageByUser(user_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.Body.Contains(searchString)
                                       || s.Title.Contains(searchString)
                                    || (s.Post != null && s.Post.Title.Contains(searchString))
                                       //|| s.Post.Body.Contains(searchString)
                                       || s.Receiver.Email.Contains(searchString)
                                       || s.Author.Email.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    messages = messages.OrderByDescending(s => s.PostId);
                    break;
                case "Date":
                    messages = messages.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    messages = messages.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    //messages = messages.OrderBy(s => s.PostId);
                    break;
            }
            //var list = messages.ToList();
            //int pageSize = (int)Math.Ceiling(messages.Count() / 20.0);
            int pageSize = 10;
            var list = messages.ToPagedList(page, pageSize);
            _service.MarkRead(list);
            if (page == list.PageCount)
            {
                page -= 1;
            }
            if (page == 0)
            {
                return RedirectToAction("ListUnread");
            }
            return RedirectToAction("ListUnread", new { sortOrder = sortOrder, currentFilter = currentFilter, page = page });
            //return View(list);
        }



        [Authorize]
        [Route("Message/ListUnread/{user_id}/{sort_order?}")]
        public ActionResult ListUnread(string user_id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var messages = _service.GetUnreadMessageByUser(user_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.Body.Contains(searchString)
                                       || s.Title.Contains(searchString)
                                    || (s.Post != null && s.Post.Title.Contains(searchString))
                                       //|| s.Post.Body.Contains(searchString)
                                       || s.Receiver.Email.Contains(searchString)
                                       || s.Author.Email.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    messages = messages.OrderByDescending(s => s.PostId);
                    break;
                case "Date":
                    messages = messages.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    messages = messages.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    //messages = messages.OrderBy(s => s.PostId);
                    break;
            }
            //var list = messages.ToList();
            //int pageSize = (int)Math.Ceiling(messages.Count() / 20.0);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            var list = messages.ToPagedList(pageNumber, pageSize);
            return View(list);
            //return View(list);
        }




        [Authorize]
        [Route("Message/List/{user_id}/{sort_order?}")]
        public ActionResult List(string user_id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var messages = _service.GetMessageByUser(user_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(s => s.Body.Contains(searchString)
                                       || s.Title.Contains(searchString)
                                       || s.Post.Title.Contains(searchString)
                                       || s.Post.Body.Contains(searchString)
                                       || s.Receiver.Email.Contains(searchString)
                                       || s.Author.Email.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    messages = messages.OrderByDescending(s => s.PostId);
                    break;
                case "Date":
                    messages = messages.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    messages = messages.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    //messages = messages.OrderBy(s => s.PostId);
                    break;
            }
            //var list = messages.ToList();
            //int pageSize = (int)Math.Ceiling(messages.Count() / 20.0);
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(messages.ToPagedList(pageNumber, pageSize));
            //return View(list);
        }


        // GET: Messages
        public ActionResult Index()
        {
            var messages = db.Messages.Include(m => m.Post);
            return View(messages.ToList());
        }

        // GET: Messages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // GET: Messages/Create
        [HttpGet]
        [Route("Message/Create/{user_id}")]
        public ActionResult Create()
        {
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title");
            return View();
        }
        [HttpGet]
        [Route("Message/PreCreate/{user_id}/{post_id}/{receiver_id}")]
        public ActionResult PreCreate(string user_id, string receiver_id, int post_id)
        {

            ViewBag.PostId = post_id.ToString();
            ViewBag.Receiver = receiver_id;
            return View("Create");
        }



        //[HttpPost]
        //[Route("Message/PreCreate/{user_id}/{post_id}/{receiver_id}")]
        //public ActionResult PreCreate(string user_id, string receiver_id, int post_id)
        //{

        //    ViewBag.PostId = post_id.ToString();
        //    ViewBag.Receiver = receiver_id;
        //    return View("Create");
        //}

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Message/Create/{user_id}")]
        public ActionResult Create([Bind(Include = "Id,Timestamp,Title,Body,IsRead,AuthorID,ReceiverId,PostId,OrderId")] Message message)
        {
            if (ModelState.IsValid)
            {
                _service.AddMessage(message);
                return RedirectToAction("ListAll", "Post");
            }

            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", message.PostId);
            return View(message);
        }

        // GET: Messages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", message.PostId);
            return View(message);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Timestamp,Title,Body,IsRead,AuthorID,ReceiverId,PostId,OrderId")] Message message)
        {
            if (ModelState.IsValid)
            {
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PostId = new SelectList(db.Posts, "Id", "Title", message.PostId);
            return View(message);
        }

        // GET: Messages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.Messages.Find(id);
            db.Messages.Remove(message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
