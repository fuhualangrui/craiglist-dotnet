﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using Data.Models;
using DB.Database;

namespace UI.Controllers
{
    /* ----------------------------------------------
     * Category Related Actions:
     * - List all categories
     * - Add a category
     * - Modify a category
     * - Hide a category
     * ----------------------------------------------
    */

    
    public class CategoryController : Controller
    {
        private readonly CategoryDbService _service = new CategoryDbService();
        private readonly SubcategoryDbService _subcat_svc = new SubcategoryDbService();
        // GET: Category/List
        [UserAuthorization("admin")]
        public ActionResult List()
        {
            var categoryList = _service.GetCategoryList();
            return View(categoryList);
        }

        // GET: Category/Add
        [UserAuthorization("admin")]
        public ActionResult Add()
        {
            return View();
        }

        // POST: Category/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorization("admin")]
        public ActionResult Add([Bind(Include = "Name, ShortName")] Category category)
        {
            if (ModelState.IsValid)
            {
                if (_service.AlreadyhastheName(category))
                {
                    ViewBag.error = "This category already been added, please try to add another category.";
                    return View(category);
                }
                if (_service.AlreadyhastheShortName(category))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(category);
                }
                _service.AddCategory(category);
                return RedirectToAction("List");
            }
            
            return View(category);
        }

        // GET: Category/Modify/5
        [UserAuthorization("admin")]
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = _service.GetCategory((int) id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        //POST: Category/Modify/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorization("admin")]
        public ActionResult Modify([Bind(Include = "Id, Name, ShortName")] Category category)
        {
            if (ModelState.IsValid)
            {
                if (_service.AlreadyhastheName(category))
                {
                    ViewBag.error = "This category already been added, please try to add another category.";
                    return View(category);
                }
                if (_service.AlreadyhastheShortName(category))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(category);
                }
                var oldCategory = _service.GetCategory(category.Id);
                oldCategory.Name = category.Name;
                oldCategory.ShortName = category.ShortName;
                _service.UpdateCategory();
                return RedirectToAction("List");
            }
            return View(category);
        }

        // GET: Category/Hide/5
        [UserAuthorization("admin")]
        public ActionResult Hide(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = _service.GetCategory((int)id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Hide", category);
        }

        // POST: Category/Hide/5
        [HttpPost, ActionName("Hide")]
        [ValidateAntiForgeryToken]
        [UserAuthorization("admin")]
        public ActionResult HideConfirmed(int id)
        {
            _service.HideCategory(id);
            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }

        [UserAuthorization("user")]
        public JsonResult GetSubList(int id)
        {
            var subcategoryList = _subcat_svc.GetSubcategoryListByCat(id);
            var data = subcategoryList.Select(subcat => new SubcategoryViewModels { Id = subcat.Id, Name = subcat.Name });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}