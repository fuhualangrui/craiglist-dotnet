﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;
using Microsoft.AspNet.Identity;

namespace UI.Controllers
{
    public class HomeController : Controller
    {
        private readonly CategoryDbService _cat_svc = new CategoryDbService();
        private readonly AreaDbService _area_svc = new AreaDbService();
        private readonly LocationDbService _loc_svc = new LocationDbService();
        private readonly SubcategoryDbService _subcat_svc = new SubcategoryDbService();
        public ActionResult Index()
        {
            var categoryList = _cat_svc.GetCategoryList();
            var areaList = _area_svc.GetAreaList();
            // Get the first area's locations
            List<Location> locationList = new List<Location>(); 
            if (areaList.Any())
            {
                locationList = _loc_svc.GetLocListWithAll(areaList.First().Id);
            }

            var viewModel = new HomeViewModels
            {
                CategoryList = categoryList,
                AreaList = areaList,
                LocationList = locationList,
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "SelectedArea,SelectedLocation,LocationList")] HomeViewModels model)
        {
            model.CategoryList = _cat_svc.GetCategoryList();
            model.AreaList = _area_svc.GetAreaList();
            var selectedArea = model.AreaList.Where(area => area.ShortName == model.SelectedArea).First();
            var locationList = _loc_svc.GetLocListWithAll(selectedArea.Id);
            model.LocationList = locationList;
            return View(model);
        }
    }
}