﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;

namespace UI.Controllers
{
    /* ---------------------------------------------
     * Response Related Actions:
     * - List all responses of current user's posts
     * - Create a response to a specific post
     * ---------------------------------------------
    */

    [UserAuthorization("user")]
    public class ResponseController : Controller
    {
        // GET: Response/List
        public ActionResult List()
        {
            return View();
        }

        // GET: Post/1/Response/Create
        [Route("Post/{id:int}/Response/Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Post/1/Response/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Post/{id:int}/Response/Create")]
        public ActionResult Create([Bind(Include = "Title,Body,PostId")] Message message)
        {
            return View(message);
        }
    }
}