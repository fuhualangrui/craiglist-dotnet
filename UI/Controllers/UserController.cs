﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;

namespace UI.Controllers
{
    /* ------------------------
     * User Related Actions:
     * - List Users
     * - Update a User
     * ------------------------
    */
    [UserAuthorization("admin")]
    public class UserController : Controller
    {   
        // GET: User/List
        public ActionResult List()
        {
            return View();
        }

        // Update the user to admin
        // GET: User/Update/1
        public ActionResult Update(int id)
        {
            return View();
        }

        // POST: User/Update/1
        [HttpPost, ActionName("Update")]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateConfirmed(int id)
        {
            return RedirectToAction("List");
        }
    }
}