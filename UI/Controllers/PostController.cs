﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;
using Microsoft.AspNet.Identity;
using PagedList;

namespace UI.Controllers
{
    /* -------------------------------
     * Post Related Actions:
     * - List current user's posts
     * - View a post
     * - Create a post
     * - Modify a post
     * - Delete a post
     * -------------------------------
    */
    public class PostController : Controller
    {
        private readonly PostDbService _post_svc = new PostDbService();
        private readonly AreaDbService _area_svc = new AreaDbService();
        private readonly LocationDbService _loc_svc = new LocationDbService();
        private readonly CategoryDbService _cat_svc = new CategoryDbService();
        private readonly SubcategoryDbService _sub_svc = new SubcategoryDbService();
        private readonly UserDbService _user_svc = new UserDbService();

        private void GenerateViewBagData()
        {
            var categoryList = _cat_svc.GetCategoryList();
            var subcategoryList = _sub_svc.GetSubcategoryListByCat(categoryList.First().Id);
            var areaList = _area_svc.GetAreaList();
            var localeList = _loc_svc.GetLocationList(areaList.First().Id);
            ViewBag.categoryList = categoryList;
            ViewBag.subcatList = subcategoryList;
            ViewBag.areaList = areaList;
            ViewBag.localeList = localeList;
        }


        // GET: Post/Index?category=category1&(subcategory=subcategory1)&area=ny&locale=bronx
        public ActionResult ListAll(string category, string subcategory, string area, string locale, int page = 1)
        {
            var areaList = _area_svc.GetAreaList();
            var categoryList = _cat_svc.GetCategoryList();
            ICollection<Location> localeList = new List<Location>();
            ICollection<Subcategory> subcatList = new List<Subcategory>();

            // Assign default value to query value if it is null
            if (categoryList.Any())
            {
                category = category ?? categoryList.First().ShortName;
                var catObj = categoryList.Where(cat => cat.ShortName == category).FirstOrDefault();
                if (catObj == null)
                {
                    return HttpNotFound();
                }
                subcatList = _sub_svc.GetSubcategoryListByCatWithAll(catObj.Id);
                if (subcatList.Any())
                {
                    subcategory = subcategory ?? subcatList.First().ShortName;
                }
            }           

            if (areaList.Any())
            {
                area = area ?? areaList.First().ShortName;
                var areaObj = areaList.Where(a => a.ShortName == area).FirstOrDefault();
                if (areaObj == null)
                {
                    return HttpNotFound();
                }
                localeList = _loc_svc.GetLocListWithAll(areaObj.Id);
                if (localeList.Any())
                {
                    locale = locale ?? localeList.First().ShortName;
                } 
            }

            var postList = _post_svc.GetPost(area, locale, category, subcategory).ToPagedList(page, 10);

            var viewModel = new CategoriesViewModels
            {
                AreaList = areaList,
                LocationList = localeList,
                CategoryList = categoryList,
                SubcategoryList = subcatList,
                PostList = postList,
                SelectedArea = area,
                SelectedLocation = locale,
                SelectedCategory = category,
                SelectedSubcategory = subcategory
            };
            if (Request.IsAjaxRequest())
            {
                return PartialView("posts", viewModel);
            }
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListAll([Bind(Include = "SelectedArea,SelectedLocation,SelectedCategory,SelectedSubcategory,LocationList,SubcategoryList")] CategoriesViewModels model)
        {
            model.AreaList = _area_svc.GetAreaList();
            model.CategoryList = _cat_svc.GetCategoryList();
            var selectedArea = model.AreaList.Where(area => area.ShortName == model.SelectedArea).First();
            var locationList = _loc_svc.GetLocListWithAll(selectedArea.Id); 
            model.LocationList = locationList.OrderBy(loc => loc.Name).ToList();
            model.SelectedLocation = model.SelectedLocation ?? model.LocationList.First().ShortName;
            var selectedCat = model.CategoryList.Where(cat => cat.ShortName == model.SelectedCategory).First();
            var subcatList = _sub_svc.GetSubcategoryListByCatWithAll(selectedCat.Id);
            model.SubcategoryList = subcatList;
            model.SelectedSubcategory = model.SelectedSubcategory ?? subcatList.First().ShortName;
            var postList = _post_svc.GetPost(model.SelectedArea, model.SelectedLocation, model.SelectedCategory, model.SelectedSubcategory).ToPagedList(1, 10);
            model.PostList = postList;
            return View(model);
        }

        private PostDbService _postService = new PostDbService();
        private CategoryDbService _categoryService = new CategoryDbService();
        [UserAuthorization("admin")]
        public ActionResult Hide(int post_id, string user_id)
        {
            Post post = _postService.GetPost(post_id);
            post.IsHidden = true;
            _postService.UpdatePost(post_id, post);
            return RedirectToAction("List", new { user_id = user_id });
        }

        [Authorize]
        [Route("Post/List/{user_id}/{sort_order?}")]
        public ActionResult List(string user_id, string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";
            if (user_id == User.Identity.GetUserId())
            {
                ViewBag.isForAdmin = false;
            } else
            {
                ViewBag.isForAdmin = true;
            }
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            var posts = _postService.GetPostByUser(user_id);
            if (!String.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(s => 
                                        s.Title.Contains(searchString)
                                       || s.Author.Email.Contains(searchString)
                                       || s.Subcategory.Name.Contains(searchString)
                                       || s.Subcategory.Category.Name.Contains(searchString)
                                       );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    posts = posts.OrderByDescending(s => s.Title);
                    break;
                case "Date":
                    posts = posts.OrderBy(s => s.Timestamp);
                    break;
                case "date_desc":
                    posts = posts.OrderByDescending(s => s.Timestamp);
                    break;
                default:
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(posts.ToPagedList(pageNumber, pageSize));
        }



        // GET: Post
        [UserAuthorization("user")]
        //[Route("Post/List/{user_id}")]
        public ActionResult ListOld(string user_id)
        {
            var posts = _postService.GetPostListByUser(user_id);
            if (user_id == User.Identity.GetUserId())
            {
                ViewBag.isForAdmin = false;
            } else
            {
                ViewBag.isForAdmin = true;
            }
            return View(posts);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = _post_svc.GetPost(id.Value);
            var location = _loc_svc.GetLocation(post.LocationId);
            var area = _area_svc.GetArea(location.AreaId);
            var category = _cat_svc.GetCategory(post.CategoryId);
            var subcategory = _sub_svc.GetSubcategory(post.SubcategoryId, post.CategoryId);
            var user = _user_svc.GetUser(post.AuthorId);
            ViewBag.area = area.ShortName;
            ViewBag.locale = location.ShortName;
            ViewBag.category = category.ShortName;
            ViewBag.subcategory = subcategory.ShortName;
            ViewBag.username = user.UserName;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        public ActionResult DetailsForPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = _post_svc.GetPost(id.Value);
            var location = _loc_svc.GetLocation(post.LocationId);
            var area = _area_svc.GetArea(location.AreaId);
            var category = _cat_svc.GetCategory(post.CategoryId);
            var subcategory = _sub_svc.GetSubcategory(post.SubcategoryId, post.CategoryId);
            var user = _user_svc.GetUser(post.AuthorId);
            ViewBag.area = area.ShortName;
            ViewBag.locale = location.ShortName;
            ViewBag.category = category.ShortName;
            ViewBag.subcategory = subcategory.ShortName;
            ViewBag.username = user.UserName;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // GET: Post/Create
        [UserAuthorization("user")]
        public ActionResult Create(string area, string locale)
        {
            int selectedAreaId;
            int selectedLocId;
            var categoryList = _cat_svc.GetCategoryList();
            var subcategoryList = _sub_svc.GetSubcategoryListByCat(categoryList.First().Id);
            var areaList = _area_svc.GetAreaList();
            ICollection<Location> localeList;
            ViewBag.categoryList = categoryList;
            ViewBag.subcatList = subcategoryList;
            ViewBag.areaList = areaList;
            if (area == null)
            {
                selectedAreaId = areaList.First().Id;
                localeList = _loc_svc.GetLocationList(selectedAreaId);
                selectedLocId = localeList.First().Id;
            }
            else
            {
                var chosenArea = _area_svc.GetAreaList().Where(a => a.ShortName == area).FirstOrDefault();
                if (chosenArea == null) selectedAreaId = areaList.First().Id;
                else selectedAreaId = chosenArea.Id;
                localeList = _loc_svc.GetLocationList(selectedAreaId);
                var chosenLoc = localeList.Where(loc => loc.ShortName == locale).FirstOrDefault();
                if (chosenLoc == null) selectedLocId = localeList.First().Id;
                else selectedLocId = chosenLoc.Id;
            }
            ViewBag.localeList = localeList;
            ViewBag.selectedAreaId = selectedAreaId;
            ViewBag.selectedLocId = selectedLocId;
            return View();
        }

        // POST: Post/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorization("user")]
        public ActionResult Create([Bind(Include = "Title,Body,SubcategoryId,CategoryId,LocationId,AreaId")] Post post)
        {
            GenerateViewBagData();
            ModelState.Remove("AuthorId");
            if (ModelState.IsValid)
            {
                post.AuthorId = User.Identity.GetUserId();
                post.Timestamp = DateTime.Now;
                post.Expiration = post.Timestamp.AddMonths(6);
                _post_svc.AddPost(post);
                return RedirectToAction("List", new { user_id = User.Identity.GetUserId() });
            }
            return View(post);
        }

        // GET: Post/Modify/5
        // Remember to check if the post is belonged to the current user if his role is not admin
        [UserAuthorization("user")]
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Post post = _post_svc.GetPost(id.Value);
            var categoryList = _cat_svc.GetCategoryList();
            var subcategoryList = _sub_svc.GetSubcategoryListByCat(post.CategoryId);
            var areaList = _area_svc.GetAreaList();
            var location = _loc_svc.GetLocation(post.LocationId);
            var localeList = _loc_svc.GetLocationList(location.AreaId);
            var user = _user_svc.GetUser(post.AuthorId);
            ViewBag.categoryList = categoryList;
            ViewBag.subcatList = subcategoryList;
            ViewBag.areaList = areaList;
            ViewBag.localeList = localeList;
            ViewBag.Area = location.Area.Id;
            ViewBag.owner = user.UserName;

            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Post/Modify/5
        // Remember to check if the post is belonged to the current user if his role is not admin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorization("user")]
        public ActionResult Modify(int id, [Bind(Include = "Title, Body, SubcategoryId, CategoryId, LocationId, AreaId")] Post post)
        {
            var categoryList = _cat_svc.GetCategoryList();
            var subcategoryList = _sub_svc.GetSubcategoryListByCat(post.CategoryId);
            var areaList = _area_svc.GetAreaList();
            var location = _loc_svc.GetLocation(post.LocationId);
            var localeList = _loc_svc.GetLocationList(location.AreaId);
            ViewBag.categoryList = categoryList;
            ViewBag.subcatList = subcategoryList;
            ViewBag.areaList = areaList;
            ViewBag.localeList = localeList;
            ViewBag.Area = location.Area.Id;
            ModelState.Remove("AuthorId");
            if (ModelState.IsValid)
            {
                post.Timestamp = DateTime.Now;
                post.Expiration = post.Timestamp.AddMonths(6);
                post.IsExpire = false;
                var subcategory = _sub_svc.GetSubcategory(post.SubcategoryId,post.CategoryId);
                post.Subcategory = subcategory;
                _post_svc.UpdatePost(id, post);
                return RedirectToAction("List", new { user_id = User.Identity.GetUserId() });
            }
            return View(post);
        }

        // GET: Post/Delete/5
        // Remember to check if the post is belonged to the current user if his role is not admin
        [UserAuthorization("user")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var post = _post_svc.GetPost((int) id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Post/Delete/5
        // Remember to check if the post is belonged to the current user if his role is not admin
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [UserAuthorization("user")]
        public ActionResult DeleteConfirmed(int id)
        {
            _post_svc.DeletePost(id);
            return RedirectToAction("List", new { user_id = User.Identity.GetUserId() });
        }

        [ActionName("CreatePosts")]
        public ActionResult CreatPosts(int n=50)
        {
            FakeModelGenerator.GeneratePostList(User.Identity.GetUserId(), n);
            return RedirectToAction("Index", "Account");
        }
    }
}
