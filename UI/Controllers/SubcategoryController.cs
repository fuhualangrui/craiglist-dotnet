﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;
using System.Net;

namespace UI.Controllers
{
    /* ------------------------------
     * Subcategory Related Actions:
     * - List all subcategories
     * - Add a subcategory
     * - Modify a subcategory
     * - Hide a subcategory
     * ------------------------------
     */
    [RoutePrefix("Category")]
    [UserAuthorization("admin")]
    public class SubcategoryController : Controller
    {
        private readonly CategoryDbService _cat_service = new CategoryDbService();
        private readonly SubcategoryDbService _subcat_service = new SubcategoryDbService();
        [HttpPost]
        [Route("Subcategory/GetByCat")]
        public JsonResult GetByCat(int catId)
        {
            var category = _cat_service.GetCategory(catId);
            var subcatList = category.Subcategories.Where(subcat => !subcat.isHidden).ToList();
            return Json(subcatList);
        }



        // GET: Category/1/Subcategory/List
        [Route("{catId:int}/Subcategory/List")]
        public ActionResult List(int catId)
        {
            var category = _cat_service.GetCategory(catId);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoryName = category.Name;
            var subcatList = category.Subcategories.Where(subcat => !subcat.isHidden).ToList();
            return View(subcatList);
        }

        // GET: Category/1/Subcategory/Add
        [Route("{catId:int}/Subcategory/Add")]
        public ActionResult Add(int catId)
        {
            var category = _cat_service.GetCategory(catId);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View();
        }

        // POST: Category/1/Subcategory/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{catId:int}/Subcategory/Add")]
        public ActionResult Add(int catId, [Bind(Include = "Name, ShortName")] Subcategory subcategory)
        {
            if (ModelState.IsValid)
            {
                if (_subcat_service.AlreadyhastheName(catId,subcategory))
                {
                    ViewBag.error = "This subcategory already been added, please try to add another subcategory.";
                    return View(subcategory);
                }
                if (_subcat_service.AlreadyhastheShortName(catId, subcategory))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(subcategory);
                }
                subcategory.CategoryId = catId;
                _subcat_service.AddSubcategory(subcategory);
                return RedirectToAction("List");
            }
            return View(subcategory);
        }

        // GET: Category/1/Subcategory/Modify/5
        [Route("{catId:int}/Subcategory/Modify/{id:int}")]
        public ActionResult Modify(int catId, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var subcategory = _subcat_service.GetSubcategory((int)id, catId);
            if (subcategory == null)
            {
                return HttpNotFound();
            }
            return View(subcategory);
        }

        //POST: Category/1/Subcategory/Modify/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{catId:int}/Subcategory/Modify/{id:int}")]
        public ActionResult Modify(int catId, [Bind(Include = "Id,CategoryId,Name,ShortName")] Subcategory subcategory)
        {
            if (ModelState.IsValid)
            {
                if (_subcat_service.AlreadyhastheName(catId, subcategory))
                {
                    ViewBag.error = "This subcategory already been added, please try to add another subcategory.";
                    return View(subcategory);
                }
                if (_subcat_service.AlreadyhastheShortName(catId, subcategory))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(subcategory);
                }
                var oldSubcategory = _subcat_service.GetSubcategory(subcategory.Id, subcategory.CategoryId);
                oldSubcategory.Name = subcategory.Name;
                oldSubcategory.ShortName = subcategory.ShortName;
                _subcat_service.UpdateSubcategory();
                return RedirectToAction("List");
            }
            return View(subcategory);
        }

        // GET: Category/1/Subcategory/Hide/5
        [Route("{catId:int}/Subcategory/Hide/{id:int}")]
        public ActionResult Hide(int catId, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var subcategory = _subcat_service.GetSubcategory((int)id, catId);
            if (subcategory == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Hide", subcategory);
        }

        // POST: Category/1/Subcategory/Hide/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("{catId:int}/Subcategory/Hide/{id:int}")]
        public ActionResult HideConfirmed(int catId, int id)
        {
            _subcat_service.HideSubcategory(id, catId);
            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }
    }
}