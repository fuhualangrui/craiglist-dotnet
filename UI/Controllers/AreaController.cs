﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;
using System.Net;

namespace UI.Controllers
{
    /* ----------------------------------------------
      * Area Related Actions:
      * - List all areas including locales
      * - Add an area
      * - Modify an area
      * - Hide an area
      * ----------------------------------------------
     */

    
    public class AreaController : Controller
    {
        private readonly AreaDbService _area = new AreaDbService();
        private readonly LocationDbService _loc = new LocationDbService();
        // GET: Area/List
        [UserAuthorization("admin")]
        public ActionResult List()
        {
            return View(_area.GetAreaList());
        }

        // GET: Area/Add
        [UserAuthorization("admin")]
        public ActionResult Add()
        {
            return View();
        }

        // POST: Area/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorization("admin")]
        public ActionResult Add([Bind(Include = "Id,Name,ShortName")] Area area)
        {
            if (ModelState.IsValid)
            {
                if (_area.AlreadyhastheName(area))
                 {
                    ViewBag.error = "This area already been added, please try to add another area.";
                    return View(area);
                 }
                if (_area.AlreadyhastheShortName(area))
                 {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(area);
                 }
                _area.AddArea(area);
                return RedirectToAction("List");
            }
            return View(area);
        }

        // GET: Area/Modify/5
        [UserAuthorization("admin")]
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = _area.GetArea(id.Value);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        //POST: Area/Modify/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [UserAuthorization("admin")]
        public ActionResult Modify([Bind(Include = "Id,Name,ShortName")] Area area)
        {
            if (ModelState.IsValid)
            {

                if (_area.AlreadyhastheName(area))
                {
                    ViewBag.error = "This area already been added, please try to add another area.";
                    return View(area);
                }
                if (_area.AlreadyhastheShortName(area))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(area);
                }
                _area.ModifyArea(area);
                return RedirectToAction("List");
            }
            return View(area);
        }

        // GET: Area/Hide/5
        [UserAuthorization("admin")]
        public ActionResult Hide(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Area area = _area.GetArea(id.Value);
            if (area == null)
            {
                return HttpNotFound();
            }
            return View(area);
        }

        // POST: Area/Hide/5
        [HttpPost, ActionName("Hide")]
        [ValidateAntiForgeryToken]
        [UserAuthorization("admin")]
        public ActionResult HideConfirmed(int id)
        {
            _area.HideArea(id);
            return RedirectToAction("List");
        }

        [UserAuthorization("user")]
        public JsonResult GetSubList(int id)
        {
            var locationList = _loc.GetLocationList(id);
            var data = locationList.Select(loc => new LocationViewModels { Id = loc.Id, Name = loc.Name });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}