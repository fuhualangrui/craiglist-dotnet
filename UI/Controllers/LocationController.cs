﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB.Database;
using Data.Models;
using System.Net;

namespace UI.Controllers
{
   /*------------------------------
    * Locale Related Actions:
    * - Add a location
    * - Modify a locale
    * - Hide a locale
    * ------------------------------
    */

    //[RoutePrefix("Area")]
    [UserAuthorization("admin")]
    public class LocationController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        LocationDbService _location = new LocationDbService();
        // GET: Area/1/List
        public ActionResult List(int areaid)
        {
            return View(_location.GetLocationList(areaid));
        }

        // GET: Area/1/Locale/Add
       // [Route("{areaId:int}/Locale/Add")]
        public ActionResult Add(int areaid)
        {
            return View();
        }

        // POST: Area/1/Locale/Add
        [HttpPost]
        [ValidateAntiForgeryToken]
       // [Route("{areaId:int}/Locale/Add")]
        public ActionResult Add(int areaid, [Bind(Include = "Id,Name,ShortName")] Location location)
        {
            if (ModelState.IsValid)
            {
                if (_location.AlreadyhastheName(areaid,location))
                {
                    ViewBag.error = "This locale already been added, please try to add another locale.";
                    return View(location);
                }
                if (_location.AlreadyhastheShortName(areaid,location))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(location);
                }
                _location.AddLocation(areaid,location);
                return RedirectToAction("List");
            }
            return View(location);
        }

        // GET: Area/1/Locale/Modify/5
        //[Route("{areaId:int}/Locale/Modify/{id:int}")]
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = _location.GetLocation(id.Value);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        //POST: Area/1/Locale/Modify/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        //[Route("{areaId:int}/locale/Modify/{id:int}")]
        public ActionResult Modify(int areaid,[Bind(Include = "Id,Name,ShortName")] Location location)
        {
            if (ModelState.IsValid)
            {
                if (_location.AlreadyhastheName(areaid, location))
                {
                    ViewBag.error = "This locale already been added, please try to add another locale.";
                    return View(location);
                }
                if (_location.AlreadyhastheShortName(areaid, location))
                {
                    ViewBag.error = "This shortname already been used, please try another shortname.";
                    return View(location);
                }
                _location.ModifyLocation(location);
                return RedirectToAction("List");
            }
            return View(location);
        }

        // GET: Area/1/Locale/Hide/5
        //[Route("{areaId:int}/Locale/Hide/{id:int}")]
        public ActionResult Hide(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location= _location.GetLocation(id.Value);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Area/1/Locale/Hide/5
        [HttpPost, ActionName("Hide")]
        [ValidateAntiForgeryToken]
        //[Route("{areaId:int}/Locale/Hide/{id:int}")]
        public ActionResult HideConfirmed(int id)
        {
            _location.HideLocation(id);
            return RedirectToAction("List");
        }
    }
}

