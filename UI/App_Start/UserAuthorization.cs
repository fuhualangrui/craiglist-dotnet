﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DB.Database;
using Microsoft.AspNet.Identity;

namespace System.Web.Mvc
{
    public class UserAuthorization : AuthorizeAttribute
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        private readonly string role;

        public UserAuthorization(string role)
        {
            this.role = role;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            string userId = httpContext.User.Identity.GetUserId();
            if (userId == null) return false;
            var user = db.Users.Find(userId);
            if (!user.IsAdmin && role.Equals("admin")) return false;
            return true;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new HttpStatusCodeResult((int) Net.HttpStatusCode.Forbidden);
            } else
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

    }
}