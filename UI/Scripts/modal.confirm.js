﻿(function () {
    var dataUrl;
    var csrfToken = $('input[name="__RequestVerificationToken"]').val();
    var confirmBtn = $('#confirm-btn');

    $('#list').on('click', function (e) {
        dataUrl = $(e.target).attr('data-url');
        if (typeof (dataUrl) !== 'undefined') {
            $.ajax({
                url: dataUrl,
                success: function (data) {
                    var content = $.parseHTML(data);
                    var body = $('#myModal .modal-body');
                    body.html(content);
                }
            })
        } 
    });

    confirmBtn.on('click', function () {
        confirmBtn.attr('disabled', true);
        $.post({
            url: dataUrl,
            data: {
                __RequestVerificationToken: csrfToken,
            },
            success: function () {
                $(myModal).modal('toggle');
                setTimeout(() => location.reload(), 500)
            },
        });
    });
})()

