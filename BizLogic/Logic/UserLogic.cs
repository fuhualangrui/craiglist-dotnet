﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace BizLogic.Logic
{
    public class UserLogic : IUserLogic
    {
        public void CancelAdmin(ApplicationUser user)
        {
            user.IsAdmin = false;
        }

        public void PromoteAdmin(ApplicationUser user)
        {
            user.IsAdmin = true;
        }
    }
}
