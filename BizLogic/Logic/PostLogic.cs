﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace BizLogic.Logic
{
    public class PostLogic : IPostLogic
    {
        public bool IsExpired(Post post)
        {
            return DateTime.Compare(post.Expiration, DateTime.Now) < 0;
        }
    }
}
