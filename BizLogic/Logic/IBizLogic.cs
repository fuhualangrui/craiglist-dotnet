﻿using Data.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizLogic.Logic
{
    // TODO: IUserLogic, IMessageLogic, IMessageDbService, IUserDbService, by Xianglong
    public interface IUserLogic
    {
        void PromoteAdmin(ApplicationUser user);
        void CancelAdmin(ApplicationUser user);
    }


    public interface IMessageLogic
    {
        void SendMessageToUser(Message message, ApplicationUser sender, ApplicationUser receiver);
        List<Message> GetUnreadMessage(ApplicationUser user);
        List<Message> GetSentMessage(ApplicationUser user);
    }

    // TODO: IPostLogic, IPostDbService, by Yang Gao
    public interface IPostLogic
    {
        bool IsExpired(Post post);
    }

    // TODO: ILocationDbService, IAreaDbService, ILocaleDbService, ICategoryDbService, ISubCategoryDbService, by Chen Yang
}
