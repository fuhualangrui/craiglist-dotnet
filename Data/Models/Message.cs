﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Message
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public bool IsRead { get; set; }

        // message <-> user : many to one
        //[Required]
        [ForeignKey("Author")]
        public string AuthorId { get; set; }
        public virtual ApplicationUser Author { get; set; }

        // message <-> receiver : many to one
        //[Required]
        [ForeignKey("Receiver")]
        public string ReceiverId { get; set; }
        public virtual ApplicationUser Receiver { get; set; }

        // Note: message may be a response to either post or message.
        // message <-> post : many to one
        [ForeignKey("Post")]
        public int PostId { get; set; }
        public virtual Post Post { get; set; }

        // message <-> message : one to one
        // Note: response to message omit
        public int OrderId { get; set; }
        //public Message ParentMessage { get; set; }


        // message <-> message : one to one
        //public int ChildMessageId { get; set; }
        //public Message ChildMessage { get; set; }
    }
}
