﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{

    public class Area
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }


        [Required]
        public string ShortName { get; set; }

        public bool Hide { get; set; }

        //[Required]
        // area <-> locale : many to one
        //public virtual ICollection<Locale> Locale { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }

    //public class Locale
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    //    public int Id { get; set; }

    //    [Required]
    //    public string Name { get; set; }

    //    [Required]
    //    public string ShortName { get; set; }

    //    [Required, ForeignKey("Area")]
    //    // locale <-> area : many to one
    //    public int AreaId { get; set; }
    //    [Required]
    //    public Area Area { get; set; }

    //}

    public class Location
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }


        [Required]
        public string ShortName { get; set; }
        public bool Hide { get; set; }
        //[Required]
        // location <-> post : one to many
        //[ForeignKey("Post")]
        //public int PostId { get; set; }
        //public Post Post { get; set; }

        [Required, ForeignKey("Area")]
        public int AreaId { get; set; }
        // location <-> area : one to one
        public virtual Area Area { get; set; }

        //[Required, ForeignKey("Locale")]
        //public int LocaleId { get; set; }
        //// location <-> locale : one to one
        //public virtual Locale Locale { get; set; }


        public virtual ICollection<Post> Posts { get; set; }
    }

}
