﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Data.Models
{
    public class HomeViewModels
    {
        [Required]
        public ICollection<Category> CategoryList { get; set; }

        [Required]
        public ICollection<Area> AreaList { get; set; }

        [Required]
        public ICollection<Location> LocationList { get; set; }

        [Display(Name = "Area")]
        public string SelectedArea { get; set; }

        [Display(Name = "Locale")]
        public string SelectedLocation { get; set; }
    }
}
