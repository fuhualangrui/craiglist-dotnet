﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Models
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public DateTime Timestamp { get; set; }
        [Required]
        public DateTime Expiration { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        //Hidden the post if location or category hidden by admin
        [Required]
        public bool IsHidden { get; set; }
        [Required]
        public bool IsExpire { get; set; }

        // post <-> location : one to many
        [Required, ForeignKey("Location")]
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }

        public int AreaId { get; set; }
        public int FakeId { get; set; }

        // post <-> user : many to one
        [Required, ForeignKey("Author")]
        public string AuthorId { get; set; }
        public virtual ApplicationUser Author { get; set; }

        // post <-> subcategory : many to one
        [Required, ForeignKey("Subcategory"), Column(Order = 0)]
        public int SubcategoryId { get; set; }
        [Required, ForeignKey("Subcategory"), Column(Order = 1)]
        public int CategoryId { get; set; }
        public virtual Subcategory Subcategory { get; set; }

        // post <-> response : one to many
        [ForeignKey("PostId")]
        public virtual ICollection<Message> Response { get; set; }

    }
}
