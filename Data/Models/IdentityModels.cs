﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    // Note: default user is used here.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Required]
        public bool IsAdmin { get; set; }

        // user <-> post : one to many
        public virtual ICollection<Post> Post { get; set; }

        // user <-> message : one to many
        //private ICollection<Message> _readMessage;
        //private ICollection<Message> _unreadMessage;
        //private ICollection<Message> _sentMessage; 
        //public virtual ICollection<Message> ReadMessage { get { return _readMessage ?? (_readMessage = new Collection<Message>()); } set { _readMessage = value; } }
        //public virtual ICollection<Message> UnreadMessage { get { return _unreadMessage ?? (_unreadMessage = new Collection<Message>()); } set { _unreadMessage = value; } }
        [ForeignKey("AuthorId")]
        public virtual ICollection<Message> SentMessages { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual ICollection<Message> ReceivedMessages { get; set; }


        //public virtual ICollection<Message> Messages { get; set; }
    }

}