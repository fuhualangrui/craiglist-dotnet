﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PagedList.Mvc;


namespace Data.Models
{
    public class CategoriesViewModels
    {
        [Required]
        public ICollection<Category> CategoryList { get; set; }

        [Required]
        public ICollection<Subcategory> SubcategoryList { get; set; }

        [Required]
        public ICollection<Area> AreaList { get; set; }

        [Required]
        public ICollection<Location> LocationList { get; set; }

        [Required]
        public PagedList.IPagedList<Post> PostList { get; set; }

        public string SelectedArea { get; set; }

        public string SelectedLocation { get; set; }

        public string SelectedCategory { get; set; }

        public string SelectedSubcategory { get; set; }
    }
}
