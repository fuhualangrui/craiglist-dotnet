namespace DB.Migrations
{
    using Data.Models;
    using DB.Database;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DB.Database.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "DB.Database.ApplicationDbContext";
        }

        private void ClearDatabase(ApplicationDbContext context)
        {
            // disable all foreign keys
            //context.Database.ExecuteSqlCommand("EXEC sp_MSforeachtable @command1 = 'ALTER TABLE ? NOCHECK CONSTRAINT all'");

            List<string> tableNames = context.Database.SqlQuery<string>("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME NOT LIKE '%Migration%'").ToList();

            for (int i = 0; tableNames.Count > 0; i++)
            {
                try
                {
                    context.Database.ExecuteSqlCommand(string.Format("DELETE FROM {0}", tableNames.ElementAt(i % tableNames.Count)));
                    tableNames.RemoveAt(i % tableNames.Count);
                    i = 0;
                }
                catch { } // ignore errors as these are expected due to linked foreign key data             
            }


            context.SaveChanges();
        }


        protected override void Seed(DB.Database.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            //context.Database.ExecuteSqlCommand("sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'");
            //context.Database.ExecuteSqlCommand("sp_MSForEachTable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[dbo].[__MigrationHistory]''),0)) DELETE FROM ?'");
            //context.Database.ExecuteSqlCommand("EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.AspNetUsers");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Categories");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Subategories");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Areas");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Locations");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Messages");
            //context.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.Posts");
            ClearDatabase(context);
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            // add areas with locations
            int numberArea = 10;
            int numberLocation = 5;
            int numberCat = 10;
            int numberSubcategory = 20;
            int numberUser = 20;
            //int numberPostEach = 40;
            int numberPost = 5000;
            context.Areas.AddRange(FakeModelGenerator.GenerateAreaList(numberArea, numberLocation, is_unique:true));
            context.SaveChanges();
            // add categories with subcategory
            context.Categories.AddRange(FakeModelGenerator.GenerateCategoryList(numberCat, numberSubcategory, is_unique:true));
            context.SaveChanges();
            // add users, all the password are the same
            // to login, use Test123456*
            FakeModelGenerator.GenerateAuthorList(numberUser);
            // add posts
            //FakeModelGenerator.GenerateEvenPostList(context, numberPostEach);
            FakeModelGenerator.GeneratePostList(context, numberPost);
            context.SaveChanges();
            context.Messages.AddRange(FakeModelGenerator.GenerateMessage(context));
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
