namespace DB.Migrations
{
    using Data.Models;
    using DB.Database;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DB.Database.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "DB.Database.ApplicationDbContext";
        }

        protected override void Seed(DB.Database.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.
            context.Database.ExecuteSqlCommand("sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'");
            context.Database.ExecuteSqlCommand("sp_MSForEachTable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[dbo].[__MigrationHistory]''),0)) DELETE FROM ?'");
            context.Database.ExecuteSqlCommand("EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'");
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            // add areas with locations
            int numberArea = 10;
            int numberLocation = 5;
            int numberCat = 10;
            int numberSubcategory = 5;
            int numberUser = 20;
            int numberPostEach = 40;
            context.Areas.AddRange(FakeModelGenerator.GenerateAreaList(numberArea, numberLocation, is_unique:true));
            context.SaveChanges();
            // add categories with subcategory
            context.Categories.AddRange(FakeModelGenerator.GenerateCategoryList(numberCat, numberSubcategory, is_unique:true));
            context.SaveChanges();
            // add users, all the password are the same
            // to login, use Test123456*
            FakeModelGenerator.GenerateAuthorList(numberUser);
            // add posts
            FakeModelGenerator.GenerateEvenPostList(context, numberPostEach);
            context.SaveChanges();
            context.Messages.AddRange(FakeModelGenerator.GenerateMessage(context));
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
