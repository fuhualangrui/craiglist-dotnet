﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace DB.Database
{
    public class CategoryDbService : ICategoryDbService
    {
        private ApplicationDbContext db;

        public CategoryDbService()
        {
            db = new ApplicationDbContext();
        }

        public CategoryDbService(ApplicationDbContext ctx)
        {
            db = ctx;
        }

        public void AddCategory(Category category)
        {
            db.Categories.Add(category);
            db.SaveChanges();
        }

        public void UpdateCategory()
        {
            db.SaveChanges();
        }
        
        public void HideCategory(int category_id)
        {
            var category = db.Categories.Find(category_id);
            foreach (var subcat in category.Subcategories)
            {
                db.Posts.RemoveRange(subcat.Post);
                subcat.isHidden = true;
            } 
            category.IsHidden = true;
            db.SaveChanges();
        }

        public Category GetCategory(int category_id)
        {
            Category category = db.Categories.Find(category_id);
            return category;
        }

        public List<Category> GetCategoryList()
        {
            var categories = from category in db.Categories
                             where !category.IsHidden
                             select category;
            return categories.ToList();
        }

        public bool AlreadyhastheName(Category thecategory)
        {
            var categories = from category in db.Categories
                             where category.Name == thecategory.Name
                             select category;
            if (categories.Count() == 0)
            {
                return false;
            }
            return true;
        }
        public bool AlreadyhastheShortName(Category thecategory)
        {
            var categories = from category in db.Categories
                             where category.ShortName == thecategory.ShortName
                             select category;
            if (categories.Count() == 0)
            {
                return false;
            }
            return true;
        }
    }
}

