﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace DB.Database
{
    public class AreaDbService : IAreaDbService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public List<Area> GetAreaList()
        {
            var areas = from area in db.Areas
                        where area.Hide != true
                        orderby area.Name
                        select area;
            return areas.ToList();
        }



        public void AddArea(Area area)
        {
            db.Areas.Add(area);
            db.SaveChanges();
        }

        public bool AlreadyhastheName(Area thearea)
        {
            var areas = from area in db.Areas
                        where area.Name == thearea.Name
                        select area;
            if (areas.Count() == 0)
            {
                return false;
            }
            return true;
        }

        public bool AlreadyhastheShortName(Area thearea)
        {
            var areas = from area in db.Areas
                        where area.ShortName== thearea.ShortName
                        select area;
            if (areas.Count() == 0)
            {
                return false;
            }
            return true;
        }
        public void ModifyArea(Area area)
        {
            Area the_area = db.Areas.Find(area.Id);
            the_area.Name= area.Name;
            the_area.ShortName = area.ShortName;
            db.SaveChanges();
        }

        public Area GetArea(int id)
        {
            Area the_area = db.Areas.Find(id);
            return the_area;
        }
        public void HideArea(int id)
        {
            Area the_area = db.Areas.Find(id);
            the_area.Hide = true;
            db.SaveChanges();
        }


    }
}