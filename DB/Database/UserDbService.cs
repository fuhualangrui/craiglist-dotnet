﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;

namespace DB.Database
{
    public class UserDbService : IUserDbService
    {
        private readonly ApplicationDbContext _context;
        public UserDbService()
        {
            _context = new ApplicationDbContext();
        }

        public List<ApplicationUser> GetOrdUsers()
        {
            var users = from u in _context.Users
                        orderby u.IsAdmin
                        select u;
            return users.ToList();
        }

        public string AddUser(ApplicationUser user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
            return user.Id;
        }

        public ApplicationUser GetUser(string user_id)
        {
            return _context.Users.Find(user_id);
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            var user = _context.Users.Where(u => u.Email.Equals(email)).Single();
            return user;
        }

        public void UpdateUser(ApplicationUser user)
        {
            _context.SaveChanges();
        }
    }
}
