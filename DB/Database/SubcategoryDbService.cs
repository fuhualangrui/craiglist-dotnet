﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace DB.Database
{
    public class SubcategoryDbService : ISubcategoryDbService
    {
        private ApplicationDbContext db;

        public SubcategoryDbService()
        {
            db = new ApplicationDbContext();
        }

        public SubcategoryDbService(ApplicationDbContext ctx)
        {
            db = ctx;
        }

        public void AddSubcategory(Subcategory subcategory)
        {
            db.Subcategories.Add(subcategory);
            db.SaveChanges();
        }

        public Subcategory GetSubcategory(int subcategory_id, int category_id)
        {
            Subcategory subcategory = db.Subcategories.Find(subcategory_id, category_id);
            return subcategory;
        }

        public void UpdateSubcategory()
        {
            db.SaveChanges();
        }

        public void HideSubcategory(int subcategory_id, int category_id)
        {
            Subcategory subcategory = db.Subcategories.Find(subcategory_id, category_id);
            subcategory.isHidden = true;
            db.SaveChanges();
        }

        public List<Subcategory> GetSubcategoryList()
        {
            var subcategories = from subcategory in db.Subcategories
                                select subcategory;
            return subcategories.ToList();
        }

        public List<Subcategory> GetSubcategoryListByCat(int category_id)
        {
            Category category = db.Categories.Find(category_id);
            if (category == null) return null;
            var subcategories = category.Subcategories;
            return subcategories.ToList();
        }

        public List<Subcategory> GetSubcategoryListByCatWithAll(int category_id)
        {
            var subcategories = GetSubcategoryListByCat(category_id);
            subcategories.Insert(0, new Subcategory { Name = "All" });
            return subcategories;
        }

        public bool AlreadyhastheName(int catid, Subcategory thesubcategory)
        {
            var subcategories = from subcategory in db.Subcategories
                                where subcategory.Name == thesubcategory.Name && subcategory.CategoryId==catid
                                select subcategory;
            if (subcategories.Count() == 0)
            {
                return false;
            }
            return true;
        }

        public bool AlreadyhastheShortName(int catid, Subcategory thesubcategory)
        {
            var subcategories = from subcategory in db.Subcategories
                                where subcategory.ShortName == thesubcategory.ShortName && subcategory.CategoryId ==catid
                                select subcategory;
            if (subcategories.Count() == 0)
            {
                return false;
            }
            return true;
        }

    }
}
