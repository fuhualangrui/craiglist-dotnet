﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace DB.Database
{
    public class MessageDbService : IMessageDbService
    {
        private readonly ApplicationDbContext _context;

        //public List<Message> GetAllMessageByPostAndAuthor(int post_id, string author_id)
        //{
        //    var list = from m in _context.Messages
        //               where m.PostId == post_id && m.AuthorID == 
        //               orderby m.Ti
        //}

        public MessageDbService()
        {
            _context = new ApplicationDbContext();
        }
        public int AddMessage(Message message)
        {
            //message.Post = _context.Posts.Find(message.PostId);
            //message.Author = _context.Users.Find(message.AuthorId);
            //message.Receiver = _context.Users.Find(message.ReceiverId);
            _context.Messages.Add(message);
            _context.SaveChanges();
            var post = message.Post;
            return message.Id;
        }

        public void DeleteMessage(int message_id)
        {
            var msg = _context.Messages.Find(message_id);
            _context.Messages.Remove(msg);
            _context.SaveChanges();
        }

        public Message GetMessage(int message_id)
        {
            return _context.Messages.Find(message_id);
        }

        public List<Message> GetMessageListByUser(string user_id)
        {
            var list = from p in _context.Messages
                       where p.AuthorId == user_id || p.ReceiverId == user_id
                       orderby p.PostId
                       orderby p.Timestamp
                       select p;
            return list.ToList();
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }

        public IQueryable<Message> GetUnreadMessageByUser(string user_id)
        {
            var list = _context.Messages.Where(p => p.ReceiverId == user_id && !p.IsRead).
                OrderBy(p => p.PostId).ThenBy(p => p.AuthorId).ThenBy(p => p.Timestamp);
            //var list = from p in _context.Messages
            //           where p.AuthorId== user_id || p.ReceiverId == user_id
            //           orderby p.PostId
            //            p.Timestamp
            //           select p;
            return list;
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }

        public void MarkRead(IEnumerable<Message> messages)
        {
            foreach(var m in messages)
            {
                m.IsRead = true;
            }
            _context.SaveChanges();
        }

     public void MarkAllRead(string user_id)
        {
            var list = _context.Messages.Where(p => p.ReceiverId == user_id);
            foreach (var m in list)
            {
                m.IsRead = true;
            }
            _context.SaveChanges();
            //var list = from p in _context.Messages
            //           where p.AuthorId== user_id || p.ReceiverId == user_id
            //           orderby p.PostId
            //            p.Timestamp
            //           select p;
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }

        public IQueryable<Message> GetReceivedMessageByUser(string user_id)
        {
            var list = _context.Messages.Where(p => p.ReceiverId == user_id).
                OrderBy(p => p.PostId).ThenBy(p => p.AuthorId).ThenBy(p => p.Timestamp);
            //var list = from p in _context.Messages
            //           where p.AuthorId== user_id || p.ReceiverId == user_id
            //           orderby p.PostId
            //            p.Timestamp
            //           select p;
            return list;
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }



        public IQueryable<Message> GetSentMessageByUser(string user_id)
        {
            var list = _context.Messages.Where(p => p.AuthorId == user_id).
                OrderBy(p => p.PostId).ThenBy(p => p.ReceiverId).ThenBy(p => p.Timestamp);
            //var list = from p in _context.Messages
            //           where p.AuthorId== user_id || p.ReceiverId == user_id
            //           orderby p.PostId
            //            p.Timestamp
            //           select p;
            return list;
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }




        public IQueryable<Message> GetMessageByUser(string user_id)
        {
            var list = _context.Messages.Where(p => p.AuthorId == user_id || p.ReceiverId == user_id).
                OrderBy(p => p.PostId).ThenBy(p => p.AuthorId).ThenBy(p => p.Timestamp);
            //var list = from p in _context.Messages
            //           where p.AuthorId== user_id || p.ReceiverId == user_id
            //           orderby p.PostId
            //            p.Timestamp
            //           select p;
            return list;
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }


        public IQueryable<Message> GetMessageContext(int message_id)
        {
            var message = _context.Messages.Find(message_id);
            var list = _context.Messages.Where(p => p.PostId == message.PostId)
                .Where(p => p.AuthorId == message.AuthorId || p.ReceiverId == message.AuthorId)
                .OrderBy(p => p.Timestamp);
            return list;
        }


        public void UpdateMessage(Message message)
        {
            _context.SaveChanges();
        }
    }
}
