using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace DB.Database
{

    // TODO: Database operations for Post
    public class PostDbService : IPostDbService
    {
        private readonly ApplicationDbContext _context;
        private readonly DbSet<Post> _table;

        public PostDbService()
        {
            _context = new ApplicationDbContext();
            _table = _context.Posts;
        }

        public PostDbService(ApplicationDbContext context)
        {
            _context = context;
            _table = _context.Posts;
        }

        public IQueryable<Post> GetPost(string area, string location, string category, string subcategory)
        {
            var selectedArea = from a in _context.Areas
                         where a.ShortName == area
                         select a;


            var selectedCategory = from a in _context.Categories
                             where a.ShortName == category
                             select a;

            var selectedLocale = from a in _context.Locations
                           where a.AreaId == selectedArea.FirstOrDefault().Id && a.ShortName == location
                           select a;

            var selectedSubcategory = from a in _context.Subcategories
                                  where a.CategoryId == selectedCategory.FirstOrDefault().Id && a.ShortName == subcategory
                                  select a;


            var posts = from post in _context.Posts
                        where post.Location.AreaId == selectedArea.FirstOrDefault().Id
                            && (selectedLocale.FirstOrDefault() == null || post.LocationId == selectedLocale.FirstOrDefault().Id)
                            && post.CategoryId == selectedCategory.FirstOrDefault().Id 
                            && (selectedSubcategory.FirstOrDefault() == null || post.SubcategoryId == selectedSubcategory.FirstOrDefault().Id)
                        orderby post.Timestamp descending
                        select post;

            return posts;

        }




        public int AddPost(Post post)
        {
            _table.Add(post);
            return _context.SaveChanges();
        }

        public int DeletePost(int post_id)
        {
            var post = _table.Find(post_id);
            _table.Remove(post);
            return _context.SaveChanges();
        }

        public Post GetPost(int post_id)
        {
            return _table.Find(post_id);
        }

        public List<Post> GetPostListByCategory(int category_id)
        {
            var category = _context.Categories.Find(category_id);
            var subcategories = category.Subcategories;
            List<Post> allPosts = new List<Post>();
            foreach (var subcategory in subcategories)
            {
                allPosts = allPosts.Concat(subcategory.Post).ToList();
            }
            return allPosts;
        }

        public List<Post> GetPostListBySubcategory(int subcategory_id, int category_id)
        {
            var subcategory = _context.Subcategories.Find(subcategory_id, category_id);
            return subcategory.Post.ToList();
        }

        public List<Post> GetPostListByUser(string user_id)
        {
            //var user = _context.Users.Find(user_id);
            var posts = from p in _context.Posts
                        where p.AuthorId == user_id && !p.IsHidden
                        orderby p.Timestamp descending
                        select p;
            return posts.ToList();
        }

        public Post UpdatePost(int post_id, Post newPost)
        {
            var oldPost = _table.Find(post_id);
            oldPost.Timestamp = newPost.Timestamp;
            oldPost.Expiration = newPost.Expiration;
            oldPost.Title = newPost.Title;
            oldPost.Body = newPost.Body;
            oldPost.CategoryId = newPost.CategoryId;
            oldPost.SubcategoryId = newPost.SubcategoryId;
            oldPost.LocationId = newPost.LocationId;
            oldPost.AreaId = newPost.AreaId;
            _context.SaveChanges();
            return oldPost;
        }

        public IQueryable<Post> GetPostByUser(string user_id)
        {
            var list = _context.Posts.Where(p => p.AuthorId == user_id).
                OrderBy(p => p.Timestamp);
            //var list = from p in _context.Messages
            //           where p.AuthorId== user_id || p.ReceiverId == user_id
            //           orderby p.PostId
            //            p.Timestamp
            //           select p;
            return list;
            //return _context.Messages.Where(s => s.AuthorID == user_id).ToList();
            //return _context.Messages.Where(s => s.Author.Id == user_id).ToList();
        }


    }
}
