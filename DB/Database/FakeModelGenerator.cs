﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Models;

namespace DB.Database
{
    public class FakeModelGenerator
    {

        private static readonly ApplicationDbContext _context = new ApplicationDbContext();
        public static int _uniqueIndex = 0;

        public static Faker<Area> fakerArea = new Faker<Area>()
            .RuleFor(a => a.Name, f => f.Address.State())
            //.RuleFor(a => a.Locations, f => fakerLocation.Generate(10))
            ;


        public static Faker<Location> fakerLocation = new Faker<Location>()
            .RuleFor(a => a.Name, f => f.Address.City())
            //.RuleFor(a => a.Area, f=>fakerArea.Generate())
            ;


        public static Faker<Subcategory> fakerSubcategory = new Faker<Subcategory>()
            .RuleFor(a => a.Name, f => f.Commerce.Categories(1).First())
            //.RuleFor(a => a.Area, f=>fakerArea.Generate())
            ;

        public static Area GenerateArea(int seed = 123)
        {
            Area area = fakerArea.Generate();
            return area;
        }

        public static List<Area> GenerateAreaList(int n, int m = 5, int seed = 123, bool is_unique = true)
        {

            var faker = new Faker<Area>()
            .RuleFor(a => a.Name, f => f.Address.State());
            //.RuleFor(a => a.Locations, f => fakerLocation.UseSeed(seed).Generate(m));
            var list = faker.UseSeed(seed).Generate(n);
            if (is_unique)
            {
                list = list.GroupBy(a => a.Name).Select(g => g.First()).ToList();
            }

            for (int i = 0; i < list.Count(); i++)
            {
                var area = list[i];
                area.ShortName = area.Name.Replace(" ", string.Empty).ToLower();
                var locations = fakerLocation.UseSeed(seed + i).Generate(m);
                if (is_unique)
                {
                    locations = locations.GroupBy(a => a.Name).Select(g => g.First()).ToList();
                }
                area.Locations = locations;
                foreach (var location in area.Locations)
                {
                    location.Area = area;
                    location.ShortName = location.Name.Replace(" ", string.Empty).ToLower();
                }
            }
            //foreach (var area in list)
            //{
            //    area.ShortName = area.Name.Replace(" ", string.Empty).ToLower();
            //}
            return list;
        }

        public static void GenerateEvenPostList(ApplicationDbContext context, int n, int seed = 123)
        {
            var authors = context.Users.ToList();
            var locations = context.Locations.ToList();
            var subcategories = context.Subcategories.ToList();
            var trueOrFalse = new[] { true, false };
            foreach (var subcat in subcategories)
            {
                foreach (var loc in locations)
                {
                    var faker = new Faker<Post>()
                    .RuleFor(a => a.Timestamp, f => f.Date.Past())
                    .RuleFor(a => a.Expiration, f => f.Date.Future())
                    .RuleFor(a => a.Title, f => f.Lorem.Sentence())
                    .RuleFor(a => a.Body, f => f.Lorem.Paragraphs())
                    .RuleFor(a => a.IsHidden, f => f.PickRandom(trueOrFalse))
                    .RuleFor(a => a.IsExpire, f => f.PickRandom(trueOrFalse))
                    .RuleFor(a => a.Location, f => loc)
                    .RuleFor(a => a.Author, f => f.PickRandom(authors))
                    .RuleFor(a => a.Subcategory, f => subcat)
                    ;
                    var list = faker.UseSeed(seed).Generate(n);
                    context.Posts.AddRange(list);
                }
            }
        }



        public static void GeneratePostList(ApplicationDbContext context, int n, int seed = 123)
        {
            var authors = context.Users.ToList();
            var locations = context.Locations.ToList();
            var subcategories = context.Subcategories.ToList();

            var trueOrFalse = new[] { true, false };
            var faker = new Faker<Post>()
            .RuleFor(a => a.Timestamp, f => f.Date.Past())
            .RuleFor(a => a.Expiration, f => f.Date.Future())
            .RuleFor(a => a.Title, f => f.Lorem.Sentence())
            .RuleFor(a => a.Body, f => f.Lorem.Paragraphs())
            .RuleFor(a => a.IsHidden, f => f.PickRandom(trueOrFalse))
            .RuleFor(a => a.IsExpire, f => f.PickRandom(trueOrFalse))
            .RuleFor(a => a.Location, f => f.PickRandom(locations))
            .RuleFor(a => a.Author, f => f.PickRandom(authors))
            .RuleFor(a => a.Subcategory, f => f.PickRandom(subcategories))
            ;
            var list = faker.UseSeed(seed).Generate(n);
            context.Posts.AddRange(list);
        }

        public static List<Message> GenerateMessage(ApplicationDbContext context, int n = 1000, int seed = 123)
        {
            var authors = context.Users.ToList();
            var posts = context.Posts.ToList();
            var trueOrFalse = new[] { true, false };
            var faker = new Faker<Message>()
                .RuleFor(a => a.Timestamp, f => f.Date.Past())
                .RuleFor(a => a.Title, f => f.Lorem.Sentence())
                .RuleFor(a => a.Body, f => f.Lorem.Paragraph())
                .RuleFor(a => a.IsRead, f => f.PickRandom(trueOrFalse))
                .RuleFor(a => a.Author, f => f.PickRandom(authors))
                .RuleFor(a => a.Post, f => f.PickRandom(posts))
                ;
            var list = faker.UseSeed(seed).Generate(n);
            foreach (var message in list)
            {
                message.Receiver = message.Post.Author;
            }
            return list;
        }
        public static void GenerateMessage(ApplicationDbContext context, int post_id, string author_id, int n = 1000, int seed = 123)
        {
            var post = context.Posts.Find(post_id);
            var author = context.Users.Find(author_id);
            var trueOrFalse = new[] { true, false };
            try
            {
                var faker = new Faker<Message>()
                    .RuleFor(a => a.Timestamp, f => f.Date.Past())
                    .RuleFor(a => a.Title, f => f.Lorem.Sentence())
                    .RuleFor(a => a.Body, f => f.Lorem.Paragraph())
                    .RuleFor(a => a.IsRead, f => f.PickRandom(trueOrFalse))
                    .RuleFor(a => a.Author, f => author)
                    .RuleFor(a => a.Post, f => post)
                    ;
                var list = faker.UseSeed(seed).Generate(n);
                foreach (var message in list)
                {
                    message.Receiver = message.Post.Author;
                }
                context.Messages.AddRange(list);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

        public static void GenerateMessageForDialog(int id, int n = 50, int seed = 123)
        {
            var message = _context.Messages.Find(id);
            var trueOrFalse = new[] { true, false };
            var authors = new[] { message.Author, message.Receiver };
            try
            {
                var faker = new Faker<Message>()
                    .RuleFor(a => a.Timestamp, f => f.Date.Past())
                    .RuleFor(a => a.Title, f => f.Lorem.Sentence())
                    .RuleFor(a => a.Body, f => f.Lorem.Paragraph())
                    .RuleFor(a => a.IsRead, f => f.PickRandom(trueOrFalse))
                    .RuleFor(a => a.Author, f => f.PickRandom(authors))
                    .RuleFor(a => a.Post, f => message.Post)
                    ;
                var list = faker.UseSeed(seed).Generate(n);
                foreach (var m in list)
                {
                    if (m.Author == authors[0])
                    {
                        m.Receiver = authors[1];
                    }
                    else
                    {
                        m.Receiver = authors[0];
                    }
                }
                _context.Messages.AddRange(list);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }





        public static void GeneratePostList(int n, int m = 5, int seed = 123)
        {
            var authors = _context.Users.ToList();
            var locations = _context.Locations.ToList();
            var subcategories = _context.Subcategories.ToList();

            var trueOrFalse = new[] { true, false };
            var faker = new Faker<Post>()
            .RuleFor(a => a.Timestamp, f => f.Date.Past())
            .RuleFor(a => a.Expiration, f => f.Date.Future())
            .RuleFor(a => a.Title, f => f.Lorem.Sentence())
            .RuleFor(a => a.Body, f => f.Lorem.Paragraphs())
            .RuleFor(a => a.IsHidden, f => f.PickRandom(trueOrFalse))
            .RuleFor(a => a.IsExpire, f => f.PickRandom(trueOrFalse))
            .RuleFor(a => a.Location, f => f.PickRandom(locations))
            .RuleFor(a => a.Author, f => f.PickRandom(authors))
            .RuleFor(a => a.Subcategory, f => f.PickRandom(subcategories))
            ;

            var list = faker.UseSeed(seed).Generate(n);

            _context.Posts.AddRange(list);
            _context.SaveChanges();
        }


        public static void GeneratePostList(string user_id, int n, int seed = 123)
        {
            var author = _context.Users.Find(user_id);
            var locations = _context.Locations.ToList();
            var subcategories = _context.Subcategories.ToList();
            var trueOrFalse = new[] { true, false };
            var faker = new Faker<Post>()
            .RuleFor(a => a.Timestamp, f => f.Date.Past())
            .RuleFor(a => a.Expiration, f => f.Date.Future())
            .RuleFor(a => a.Title, f => f.Lorem.Sentence())
            .RuleFor(a => a.Body, f => f.Lorem.Paragraphs())
            .RuleFor(a => a.IsHidden, f => f.PickRandom(trueOrFalse))
            .RuleFor(a => a.IsExpire, f => f.PickRandom(trueOrFalse))
            .RuleFor(a => a.Location, f => f.PickRandom(locations))
            .RuleFor(a => a.Author, f => author)
            .RuleFor(a => a.Subcategory, f => f.PickRandom(subcategories))
            ;

            var list = faker.UseSeed(seed).Generate(n);

            _context.Posts.AddRange(list);
            _context.SaveChanges();
        }


        public static void GenerateAuthorList(int n, int seed = 123)
        {

            var trueOrFalse = new[] { true, false };
            var faker = new Faker<ApplicationUser>()
            .RuleFor(a => a.UserName, f => f.Internet.UserName())
            .RuleFor(a => a.Email, f => f.Internet.Email())
            .RuleFor(a => a.IsAdmin, f => f.PickRandom(trueOrFalse))
            ;

            var list = faker.UseSeed(seed).Generate(n);

            foreach (var user in list)
            {
                var userStore = new UserStore<ApplicationUser>(_context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                userManager.Create(user, "Test123456*");
            }

        }


        public static List<Category> GenerateCategoryList(int n, int m = 5, int seed = 123, bool is_unique = false)
        {
            var faker = new Faker<Category>()
            .RuleFor(a => a.Name, f => f.Commerce.Categories(1).First());
            //.RuleFor(a => a.Subcategories, f => fakerSubcategory.UseSeed(seed).Generate(m));

            var list = faker.UseSeed(seed).Generate(n);

            if (is_unique)
            {
                list = list.GroupBy(a => a.Name).Select(g => g.First()).ToList();
            }

            for (int i = 0; i < list.Count(); i++)
            {
                var category = list[i];
                category.ShortName = category.Name.Replace(" ", string.Empty).ToLower();
                var subcategories = fakerSubcategory.UseSeed(seed + i).Generate(m);
                if (is_unique)
                {
                    subcategories = subcategories.GroupBy(a => a.Name).Select(g => g.First()).ToList();
                }
                category.Subcategories = subcategories;
                foreach (var subcategory in category.Subcategories)
                {
                    subcategory.Category = category;
                    subcategory.ShortName = subcategory.Name.Replace(" ", string.Empty).ToLower();
                }
            }
            return list;
        }




        //public static Location GenerateLocation(int seed = 123)
        //{
        //    Location subcategory = fakerLocation.Generate();
        //    return subcategory;
        //}
    }
}
