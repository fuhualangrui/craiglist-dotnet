﻿using Data.Models;
using Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB.Database
{
    public interface IPostDbService
    {
        // Add a new post to database.
        int AddPost(Post post);
        Post GetPost(int post_id);
        Post UpdatePost(int post_id, Post newPost);
        int DeletePost(int post_id);
        List<Post> GetPostListByUser(string user_id);
        List<Post> GetPostListByCategory(int category_id);
        List<Post> GetPostListBySubcategory(int subcategory_id, int category_id);
    }

    public interface IMessageDbService
    {
        // Add a new message to database.
        int AddMessage(Message message);
        Message GetMessage(int message_id);
        void UpdateMessage(Message message);
        void DeleteMessage(int message_id);
        List<Message> GetMessageListByUser(string user_id);
    }

    public interface IUserDbService
    {
        // Add a new user to database.
        string AddUser(ApplicationUser user);
        ApplicationUser GetUser(string user_id);
        void UpdateUser(ApplicationUser user);
    }

    public interface ICategoryDbService
    {
        void AddCategory(Category category);
        void UpdateCategory();
        void HideCategory(int category_id);
        Category GetCategory(int category_id);
        List<Category> GetCategoryList();
    }

    public interface ISubcategoryDbService
    {
        void AddSubcategory(Subcategory subcategory);
        Subcategory GetSubcategory(int subcategory_id, int category_id);
        List<Subcategory> GetSubcategoryList();
        List<Subcategory> GetSubcategoryListByCat(int category_id);
    }

    public interface ILocationDbService
    {
        void AddLocation(int areaid,Location location);
        Location GetLocation(int location_id);
        List<Location> GetLocationList(int area_id);
        List<Location> GetLocationList();
    }

    public interface IAreaDbService
    {
        List<Area> GetAreaList();
    }

    public interface ILocaleDbService
    {
        List<Area> GetLocaleList(int area_id);
    }
}
