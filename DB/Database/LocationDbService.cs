﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;

namespace DB.Database
{
    public class LocationDbService : ILocationDbService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public void AddLocation(int areaid, Location location)
        {
            location.AreaId = areaid;
            db.Locations.Add(location);
            db.SaveChanges();
        }

        public bool AlreadyhastheName(int areaid, Location thelocation)
        {
            var locations = from location in db.Locations
                            where location.Name == thelocation.Name && location.AreaId==areaid
                            select location;
            if (locations.Count() == 0)
            {
                return false;
            }
            return true;
        }

        public bool AlreadyhastheShortName(int areaid, Location thelocation)
        {
            var locations = from location in db.Locations
                            where location.ShortName == thelocation.ShortName && location.AreaId == areaid
                            select location;
            if (locations.Count() == 0)
            {
                return false;
            }
            return true;
        }
        public Location GetLocation(int location_id)
        {
            Location location = db.Locations.Find(location_id);
            return location;
        }

        public void ModifyLocation(Location location)
        {
            Location the_location = db.Locations.Find(location.Id);
            the_location.Name = location.Name;
            the_location.ShortName = location.ShortName;
            db.SaveChanges();
        }

        public List<Location> GetLocationList(int area_id)
        {
            var locations = from location in db.Locations
                            where location.AreaId == area_id && location.Hide!=true
                            orderby location.Name
                            select location;
            
            return locations.ToList();
        }

        public List<Location> GetLocListWithAll(int area_id)
        {
            var locations = GetLocationList(area_id);
            locations.Insert(0, new Location { Name = "All" });
            return locations;
        }



        public List<Location> GetLocationList()
        {
            var locations = from location in db.Locations
                            select location;
            return locations.ToList();
        }

        public void HideLocation(int id)
        {
            Location location = db.Locations.Find(id);
            db.Posts.RemoveRange(location.Posts);
            location.Hide = true;
            db.SaveChanges();
        }

    }
}

