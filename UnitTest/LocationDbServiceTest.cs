﻿using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DB.Database;
using Models;
using Data.Models;
using System;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class LocationDbServiceTest
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        LocationDbService test = new LocationDbService();
        [TestMethod]
        public void TestAddLocation()
        {
            Location the_location = new Location()
            {
                Id = 2,
                Name = "Manhattan",
                ShortName = "mannt"
            };
            test.AddLocation(1,the_location);
            var locations = from location in db.Locations
                            where location.Id == 2
                            select location;
            Assert.AreEqual(locations.Count(), 1);


        }

        [TestMethod]
        public void TestGetLocation()
        {
            Location location = db.Locations.Find(2);
            Assert.AreEqual(location.Id, 2);
        }
    }
}

