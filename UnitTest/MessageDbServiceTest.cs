﻿using Data.Models;
using DB.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class MessageDbServiceTest
    {
        private readonly MessageDbService _service;
        public MessageDbServiceTest()
        {
            _service = new MessageDbService();
        }


        [TestMethod]
        public void TestAddMessage()
        {
            var message = new Message();
            message.Timestamp = DateTime.UtcNow;
            message.Title = "test";
            message.Body = "test";
            _service.AddMessage(message);
            Assert.IsNotNull(message.Id, "Didn't add the message successfully!");
        }

        [TestMethod]
        public void TestDeleteMessage()
        {
            var message = new Message();
            message.Timestamp = DateTime.UtcNow;
            message.Title = "test";
            message.Body = "test";
            _service.AddMessage(message);
            var id = message.Id;
            _service.DeleteMessage(id);
            Assert.IsNull(_service.GetMessage(id), "Didn't delete the message successfully!");
        }

        [TestMethod]
        public void TestGetMessage()
        {
            var message = new Message();
            message.Timestamp = DateTime.UtcNow;
            message.Title = "test";
            message.Body = "test";
            _service.AddMessage(message);
            var id = message.Id;
            message = _service.GetMessage(id);
            Assert.AreEqual(message.Id, id, "Didn't get the message successfully!");
        }

        [TestMethod]
        public void TestUpdateAddMessage()
        {
            var message = new Message();
            message.Timestamp = DateTime.UtcNow;
            message.Title = "test";
            message.Body = "test";
            _service.AddMessage(message);
            var id = message.Id;
            var user = new Models.ApplicationUser { UserName = "message test", PasswordHash = "message test", IsAdmin = false };
            message.Author = user;
            _service.UpdateMessage(message);
            Assert.AreEqual(message.Author, user, "Didn't get the message successfully!");
        }


    }
}
