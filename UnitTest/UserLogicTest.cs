﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB.Database;
using Data.Models;
using BizLogic.Logic;
using Models;

namespace UnitTest
{
    [TestClass]
    public class UserLogicTest
    {

        private readonly UserDbService _service;
        private readonly UserLogic _logic;
        public UserLogicTest()
        {
            _service = new UserDbService();
            _logic = new UserLogic();
        }

        [TestMethod]
        public void TestCancelAdmin()
        {
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = true };
            _service.AddUser(user);
            _logic.CancelAdmin(user);
            _service.UpdateUser(user);
            Assert.AreEqual(false, user.IsAdmin, "CancelAdmin failed!");
        }



        [TestMethod]
        public void TestPromoteAdmin()
        {
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = false };
            _service.AddUser(user);
            _logic.PromoteAdmin(user);
            _service.UpdateUser(user);
            Assert.AreEqual(true, user.IsAdmin, "PromoteAdmin failed!");
        }

    }
}
