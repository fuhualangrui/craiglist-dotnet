﻿using BizLogic.Logic;
using Data.Models;
using DB.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class MessageLogicTest
    {

        private readonly UserDbService user_service;
        private readonly MessageDbService msg_service;
        private readonly MessageLogic msg_logic;
        public MessageLogicTest()
        {
            user_service = new UserDbService();
            msg_service = new MessageDbService();
            msg_logic = new MessageLogic();
        }

        [TestMethod]
        public void TestGetSentMessage()
        {
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = false };
            user_service.AddUser(user);
            var sent_messages = msg_logic.GetSentMessage(user);
            Assert.IsNotNull(sent_messages, "Fail to get sent messages!");
        }



        [TestMethod]
        public void TestGetUnreadMessage()
        {
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = false };
            user_service.AddUser(user);
            var unread_messages = msg_logic.GetUnreadMessage(user);
            Assert.IsNotNull(unread_messages, "Fail to get unread messages!");
        }

        [TestMethod]
        public void TestSendMessageToUser()
        {
            var sender_email = TestUtils.RandomString(10) + "@gmail.com";
            var sender = new ApplicationUser { Email = sender_email, UserName = sender_email, PasswordHash = "Test", IsAdmin = false };
            var receiver_email = TestUtils.RandomString(10) + "@gmail.com";
            var receiver = new ApplicationUser { Email = receiver_email, UserName = receiver_email, PasswordHash = "Test", IsAdmin = false};
            user_service.AddUser(sender);
            user_service.AddUser(receiver);
            var message = new Message { Timestamp = DateTime.UtcNow, Title = "Test SendMessageToUser", Body="Test SendMessageToUser" };
            msg_logic.SendMessageToUser(message, sender, receiver);
            msg_service.UpdateMessage(message);
            Assert.AreEqual(message.Receiver, receiver, "SendMessageToUser failed!");
            Assert.AreEqual(message.Author, sender, "SendMessageToUser failed!");
        }

    }
}
