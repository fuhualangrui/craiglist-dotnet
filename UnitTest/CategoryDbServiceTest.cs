﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DB.Database;
using Data.Models;
using System;
using System.Linq;
using System.Data.Entity;


namespace UnitTest
{
    [TestClass]
    public class CategoryDbServiceTest
    {
        private CategoryDbService _service;
        private ApplicationDbContext _context;
        private DbSet<Category> _table;

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new ApplicationDbContext();
            _table = _context.Categories;
            _service = new CategoryDbService(_context);

            // Remove all rows in Category table
            _context.Database.ExecuteSqlCommand("delete from Categories;dbcc checkident('Categories',reseed,0)");

            // Create two categories
            _table.Add(new Category { Name = "community", ShortName = "cmty" });
            _table.Add(new Category { Name = "housing", ShortName = "hs" });
            _context.SaveChanges();
        }

        [TestMethod]
        public void TestAddCategory()
        {
            int beforeAddCount = _table.Count();
            var newCategory = new Category { Name = "jobs", ShortName = "jobs" }; 
            _service.AddCategory(newCategory);
            int afterAddCount = _table.Count();
            Assert.AreEqual(beforeAddCount + 1, afterAddCount);
            var categories = from category in _table
                             where category.Id == newCategory.Id
                             select category;
            Assert.AreEqual(categories.Count(), 1);
        }

        [TestMethod]
        public void TestGetCategory()
        {
            Category category = _service.GetCategory(1);
            Assert.AreEqual(1, category.Id);
        }

        [TestMethod]
        public void TestGetCategoryList()
        {
            var categoryList = _service.GetCategoryList();
            Assert.AreEqual(2, categoryList.Count);
            Assert.IsTrue(categoryList.Any(cat => cat.Name == "community"));
            Assert.IsTrue(categoryList.Any(cat => cat.Name == "housing"));
            // A hidden category should not appear in the list
            var categroy1 = _table.Find(1);
            categroy1.IsHidden = true;
            _context.SaveChanges();
            categoryList = _service.GetCategoryList();
            Assert.IsFalse(categoryList.Any(cat => cat.Name == "community"));
        }

        [TestMethod]
        public void TestUpdateCategory()
        {
            var originalCat = _table.Find(1);
            Assert.AreEqual("community", originalCat.Name);
            originalCat.Name = "services";
            _service.UpdateCategory();
            Assert.AreEqual("services", originalCat.Name);
        }

        [TestMethod]
        public void TestHideCategory()
        {
            var category = _context.Categories.Find(2);
            Assert.IsFalse(category.IsHidden);
            _service.HideCategory(2);
            Assert.IsTrue(category.IsHidden);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _context.Dispose();
            _context = null;
        }

    }
}
