﻿using DB.Database;
using Data.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class FakeModelGeneratorTest
    {
        private ApplicationDbContext _context;
        private readonly bool _isClean = false;

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new ApplicationDbContext();
        }

        [TestMethod]
        public void TestAddPost()
        {

            int n = _context.Posts.Count();
            FakeModelGenerator.GeneratePostList(20);
            //_context.Posts.AddRange(FakeModelGenerator.GeneratePostList(_context, 20));
            //_context.SaveChanges();
            Assert.AreEqual(_context.Posts.Count(), n + 20);
            try
            {
            }
            catch (Exception exception)
            {
                Assert.Fail();
                Console.WriteLine(exception);
            }
        }



        [TestMethod]
        public void TestAddCategory()
        {
            try
            {
                int n = _context.Categories.Count();
                _context.Categories.AddRange(FakeModelGenerator.GenerateCategoryList(20));
                _context.SaveChanges();
                Assert.AreEqual(_context.Categories.Count(), n + 20);
            }
            catch (Exception exception)
            {
                Assert.Fail();
                Console.WriteLine(exception);
            }
        }



        [TestMethod]
        public void TestAddArea()
        {
            try
            {
                int n = _context.Areas.Count();
                _context.Areas.AddRange(FakeModelGenerator.GenerateAreaList(20));
                _context.SaveChanges();
                Assert.AreEqual(_context.Areas.Count(), n + 20);
            }
            catch (Exception exception)
            {
                Assert.Fail();
                Console.WriteLine(exception);
            }
        }

        [TestMethod]
        public void TestAddAuthor()
        {
            try
            {
                int n = _context.Users.Count();
                FakeModelGenerator.GenerateAuthorList(20);
                Assert.AreEqual(_context.Users.Count(), n + 20);
            }
            catch (Exception exception)
            {
                Assert.Fail();
                Console.WriteLine(exception);
            }
        }



        [TestCleanup]
        public void TestCleanUp()
        {
            //var all = from c in _context.Areas select c;
            //_context.Areas.RemoveRange(all);
            if (_isClean)
            {
                _context.Database.ExecuteSqlCommand("sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'");
                _context.Database.ExecuteSqlCommand("sp_MSForEachTable 'IF OBJECT_ID(''?'') NOT IN (ISNULL(OBJECT_ID(''[dbo].[__MigrationHistory]''),0)) DELETE FROM ?'");
                _context.Database.ExecuteSqlCommand("EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'");
            }
            _context.SaveChanges();
        }


    }
}
