﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Data.Models;
using DB.Database;
using System.Linq;
using System.Data.Entity;
using Models;

namespace UnitTest
{
    [TestClass]
    public class PostDbServiceTest
    {
        private PostDbService _service;
        private UserDbService user_service;
        private ApplicationDbContext _context;
        private DbSet<Post> _table;
        private ApplicationUser _user;
        //private string userId;

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new ApplicationDbContext();
            _table = _context.Posts;
            _service = new PostDbService(_context);
            user_service = new UserDbService();

            // Remove all rows in related tables
            _context.Database.ExecuteSqlCommand("delete from Posts;dbcc checkident('Posts',reseed,0)");
            _context.Database.ExecuteSqlCommand("delete from Subcategories;dbcc checkident('Subcategories',reseed,0)");
            _context.Database.ExecuteSqlCommand("delete from Categories;dbcc checkident('Categories',reseed,0)");

            // Create a category
            var newCategory = new Category { Name = "community", ShortName = "cmt" };
            _context.Categories.Add(newCategory);
            _context.SaveChanges();

            // Create a subcategory
            var newSubcategory1 = new Subcategory { Name = "events", ShortName = "evt", CategoryId = 1 };
            var newSubcategory2 = new Subcategory { Name = "groups", ShortName = "gp", CategoryId = 1 };
            _context.Subcategories.Add(newSubcategory1);
            _context.Subcategories.Add(newSubcategory2);
            _context.SaveChanges();


            var email = TestUtils.RandomString(10) + "@gmail.com";
            _user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = true };
            user_service.AddUser(_user);

            // Initiate with six posts
            for (int i = 1; i < 7; ++i)
            {
                var newPost = new Post
                {
                    Timestamp = DateTime.Now,
                    Expiration = DateTime.Now.AddDays(1),
                    Title = $"Post{i}",
                    Body = $"This is the Post{i}",
                    IsExpire = false,
                    IsHidden = false,
                    AuthorId = _user.Id,
                    CategoryId = 1,
                    SubcategoryId = i % 2 + 1,
                };
                _table.Add(newPost);
            }
            _context.SaveChanges();
        }

        [TestMethod]     
        public void TestAddPost()
        {
            int beforeAddCount = _table.Count();
            var newPost = new Post {
                Timestamp = DateTime.Now,
                Expiration = DateTime.Now.AddDays(1),
                Title = "Post4",
                Body = "This is the Post4",
                IsExpire = false,
                IsHidden = false,
                //AuthorId = "232582d4-4cf8-4a56-8f96-401a2af2d7d6",
                    AuthorId = _user.Id,
                    //Author = _user,
                CategoryId = 1,
                SubcategoryId = 1,
            };
            _service.AddPost(newPost);
            int afterAddCount = _table.Count();
            Assert.AreEqual(beforeAddCount + 1, afterAddCount);
            bool isExist = _table.Any(post => post.Id == newPost.Id);
            Assert.IsTrue(isExist);
        }

        [TestMethod]
        public void TestDeletePost()
        {
            int beforeDeleteCount = _table.Count();
            int deletePostId = _table.First().Id;
            // Try delete the first Post
            _service.DeletePost(deletePostId);
            int afterDeleteCount = _table.Count();
            Assert.AreEqual(beforeDeleteCount - 1, afterDeleteCount);
            bool isExist = _table.Any(post => post.Id == deletePostId);
            Assert.IsFalse(isExist);
        }

        [TestMethod]
        public void TestGetPost()
        {
            // Get the second post
            var post = _service.GetPost(2);
            Assert.AreEqual(2, post.Id);
        }

        [TestMethod]
        public void TestUpdatePost()
        {
            var targetPost = new Post
            {
                Timestamp = DateTime.Now,
                Expiration = DateTime.Now.AddDays(1),
                Title = "Updated Title",
                Body = "This is a modified post",
                IsExpire = false,
                IsHidden = false,
                //AuthorId = "232582d4-4cf8-4a56-8f96-401a2af2d7d6",
                    AuthorId = _user.Id,
                    Author = _user,
                CategoryId = 1,
                SubcategoryId = 2
            };
            var oldTimestamp = _table.Find(1).Timestamp;
            _service.UpdatePost(1, targetPost);
            var updatedPost = _table.Find(1);
            Assert.AreEqual("Updated Title", updatedPost.Title);
            Assert.AreEqual("This is a modified post", updatedPost.Body);
            Assert.AreNotEqual(oldTimestamp, updatedPost.Timestamp);
        }

        [TestMethod]
        public void TestGetPostListByCategory()
        {
            var posts = _service.GetPostListByCategory(1);
            Assert.AreEqual(6, posts.Count);
            Assert.IsTrue(posts.Any(post => post.Title == "Post2"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post3"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post4"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post5"));
        }

        [TestMethod]
        public void TestGetPostListBySubcategory()
        {
            // get posts in community/events
            var posts = _service.GetPostListBySubcategory(1, 1);
            Assert.AreEqual(3, posts.Count);
            Assert.IsTrue(posts.Any(post => post.Title == "Post2"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post4"));
            Assert.IsFalse(posts.Any(post => post.Title == "Post3"));
        }

        [TestMethod]
        public void TestGetPostListByUser()
        {
            var posts = _service.GetPostListByUser(_user.Id);
            //var posts = _service.GetPostListByUser("9fdcd4b9-eb90-4875-be0a-699bdf5a69ce");
            Assert.AreEqual(6, posts.Count);
            Assert.IsTrue(posts.Any(post => post.Title == "Post1"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post3"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post4"));
            Assert.IsTrue(posts.Any(post => post.Title == "Post6"));
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _context.Dispose();
            _context = null;
        }
    }
}
