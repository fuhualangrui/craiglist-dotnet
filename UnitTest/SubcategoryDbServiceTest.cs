﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DB.Database;
using Data.Models;
using System.Linq;
using System.Data.Entity;


namespace UnitTest
{
    [TestClass]
    public class SubcategoryDbServiceTest
    {
        private SubcategoryDbService _service;
        private ApplicationDbContext _context;
        private DbSet<Subcategory> _table;

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new ApplicationDbContext();
            _table = _context.Subcategories;
            _service = new SubcategoryDbService(_context);

            // Remove all rows in Category table
            _context.Database.ExecuteSqlCommand("delete from Subcategories;dbcc checkident('Subcategories',reseed,0)");

            // Create two category
            _context.Categories.Add(new Category { Name = "community", ShortName = "cmmt" });
            _context.Categories.Add(new Category { Name = "housing", ShortName = "hs" });
            _context.SaveChanges();

            // Create two subcategories
            _table.Add(new Subcategory { Name = "events", ShortName = "events" , CategoryId = 1 });
            _table.Add(new Subcategory { Name = "groups", ShortName = "groups", CategoryId = 1 });
            _table.Add(new Subcategory { Name = "pets", ShortName = "pets", CategoryId = 2 });
            _context.SaveChanges();
        }

        [TestMethod]
        public void TestAddSubcategory()
        {
            var newSubcategory = new Subcategory { Name = "activities", ShortName = "act", CategoryId = 1 };
            int beforeAddCount = _table.Count();
            _service.AddSubcategory(newSubcategory);
            int afterAddCount = _table.Count();
            Assert.AreEqual(beforeAddCount + 1, afterAddCount);
            var subcategories = from subcategory in _table
                                where subcategory.Id == newSubcategory.Id
                                select subcategory;
            Assert.AreEqual(subcategories.Count(), 1);
        }

        [TestMethod]
        public void TestGetSubcategory()
        {
            Subcategory subcategory = _service.GetSubcategory(2, 1);
            Assert.AreEqual(2, subcategory.Id);
            Assert.AreEqual("groups", subcategory.Name);
        }

        [TestMethod]
        public void TestUpdateSubcategory()
        {
            Subcategory subcategory = _service.GetSubcategory(1, 1);
            Assert.AreEqual("events", subcategory.Name);
            subcategory.Name = "housing";
            _service.UpdateSubcategory();
            Assert.AreEqual("housing", subcategory.Name);
        }

        [TestMethod]
        public void TestGetSubcategoryListByCat()
        {
            var subcatList = _service.GetSubcategoryListByCat(1);
            Assert.IsTrue(subcatList.Any(subcat => subcat.Name == "events"));
            Assert.IsTrue(subcatList.Any(subcat => subcat.Name == "groups"));
            Assert.IsFalse(subcatList.Any(subcat => subcat.Name == "pets"));
        }

        [TestMethod]
        public void TestHideSubcategory()
        {
            Assert.IsFalse(_table.Find(1, 1).isHidden);
            _service.HideSubcategory(1, 1);
            Assert.IsTrue(_table.Find(1, 1).isHidden);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            _context.Dispose();
            _context = null;
        }

    }
}
