﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using DB.Database;
using Models;

namespace UnitTest
{
    /// <summary>
    /// Summary description for UserDbService
    /// </summary>
    [TestClass]
    public class UserDbServiceTest
    {
        private readonly UserDbService _service;
        public UserDbServiceTest()
        {
            _service = new UserDbService();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestAddUser()
        {
            var password = "test";
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { UserName = email, Email = email, PasswordHash=password, IsAdmin = false};
            _service.AddUser(user);
            Assert.IsNotNull(user.Id, "Didn't add the user successfully!");
        }

        [TestMethod]
        public void TestGetUser()
        {
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = false };
            _service.AddUser(user);
            var id = user.Id;
            user = _service.GetUser(id);
            Assert.AreEqual(id, user.Id);
        }

        [TestMethod]
        public void TestUpdateUser()
        {
            var email = TestUtils.RandomString(10) + "@gmail.com";
            var user = new ApplicationUser { Email = email, UserName = email, PasswordHash = "Test", IsAdmin = false };
            _service.AddUser(user);
            var id = user.Id;
            user = _service.GetUser(id);
            user.IsAdmin = true;
            _service.UpdateUser(user);
            user = _service.GetUser(id);
            Assert.AreEqual(true, user.IsAdmin);
        }


    }
}
